package nathan.piranda;


import java.io.IOException;
import java.util.Scanner;

public class App
{
    public static void main( String[] args ) throws IOException {
        System.out.println("** BIENVENUE DANS L'APPLICATION **");
        Teacher teacher = null;
        Admin admin = null;
        Student student = null;

        int profile = MenuLibrary.profileChoice();

        switch (profile){
            case 1:
                String teacherLogin = LoginLibrary.loginTeacher();
                teacher = new Teacher(teacherLogin);
                break;
            case 2:
                LoginLibrary.loginAdmin();
                admin = new Admin();
                break;
            case 3:
                String[] studentLogin = LoginLibrary.loginStudent();
                student = new Student(studentLogin[0],studentLogin[1],studentLogin[2]);

        }

        int menuChoice = 0;
        do{
            switch (profile){
                case 1:
                    menuChoice = MenuLibrary.teacherMenu();
                    break;
                case 2:
                    menuChoice = MenuLibrary.adminMenu();
                    break;
                case 3:
                    menuChoice = MenuLibrary.studentMenu();
                    break;
            }

            if (profile == 1 && menuChoice == 1){
                System.out.println("** SAISIE D'UNE ABSENCE **");
                JSONLibrary.createAbsence();
            }

            if ((profile == 1 && menuChoice == 2) || (profile == 2 && menuChoice == 1)){
                System.out.println("** CONSULTATION D'UN PROFIL **");
                Scanner scanner = new Scanner(System.in);
                System.out.print("Saisissez le nom de l'étudiant : ");
                String target = scanner.nextLine();
                JSONLibrary.checkStudent(target);
            }

            if (profile == 2 && menuChoice == 2){
                System.out.println("** CONSULTATION GLOBALE DES ABSENCES **");
                JSONLibrary.checkGlobal();
            }

            if (profile == 3 && menuChoice == 1){
                System.out.println("** CONSULTATION DU PROFIL "+ student.getName().toUpperCase() +" **");
                JSONLibrary.checkStudent(student.getName());
            }

        } while(menuChoice != 3);

    }
}
