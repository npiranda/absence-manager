package nathan.piranda;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class JSONLibrary {

    /**
     * Charge le fichier d'absence JSON ou créé un nouveau fichier JSON s'il n'existe pas ou est vide.
     * @return Le fichier JSON parsé.
     */
    public static Object loadJSON(){
        JSONParser jsonParser = new JSONParser();
        File file = new File("test.json");
        /*
         * Initialize empty JSON if doesnt exist
         */
        if (!file.exists()){
            try {
                boolean isCreated = file.createNewFile();
                if (isCreated){
                    JSONArray jsonArray = new JSONArray();
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write(jsonArray.toJSONString());
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(FileReader reader = new FileReader(file)){
            return jsonParser.parse(reader);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Fonctionnalité de vérification des taux d'absences de tous les étudiants.
     */
    public static void checkGlobal() {
        Object object = loadJSON();
        JSONArray jsonArray = (JSONArray) object;

        assert jsonArray != null;
        for (Object entry: jsonArray) {
            JSONObject absence = (JSONObject) entry;
            JSONObject absenceObject = (JSONObject) absence.get("absence");
            String firstName = (String) absenceObject.get("firstName");
            System.out.print(firstName + " ");
            String lastName = (String) absenceObject.get("lastName");
            System.out.print(lastName + " ");
            int duration = Integer.parseInt(String.valueOf(absenceObject.get("duration")));
            System.out.println(duration);
        }
    }


    /**
     * Fonctionnalité de création d'une absence dans le fichier JSON.
     * @throws IOException Exception sur le fichier JSON
     */
    public static void createAbsence() throws IOException {
        Object object = loadJSON();
        JSONArray jsonArray = (JSONArray) object;
        JSONObject newEntryDetails = new JSONObject();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Saisissez le prénom de l'élève : ");
        String firstName = scanner.nextLine();
        System.out.print("Saisissez le nom de l'élève : ");
        String lastName = scanner.nextLine();
        System.out.print("Saisissez la durée de l'absence : ");
        int duration = scanner.nextInt();

        newEntryDetails.put("firstName",firstName);
        newEntryDetails.put("lastName",lastName);
        newEntryDetails.put("duration",duration);

        JSONObject newEntry = new JSONObject();
        newEntry.put("absence",newEntryDetails);

        assert jsonArray != null;
        jsonArray.add(newEntry);

        FileWriter file = new FileWriter("test.json");
        file.write(jsonArray.toJSONString());
        System.out.println("** L'ABSENCE A BIEN ÉTÉ PRISE EN COMPTE. **");
        file.flush();
        file.close();
    }

    /**
     * Fonctionnalité de consultation du taux d'absence d'un étudiant précis
     * @param target Le nom de l'étudiant
     */
    public static void checkStudent(String target) {
        Object object = loadJSON();
        JSONArray jsonArray = (JSONArray) object;
        int sumAbsence = 0;
        int nbReport = 0;


        assert jsonArray != null;
        for (Object entry: jsonArray) {
            JSONObject absence = (JSONObject) entry;
            JSONObject absenceObject = (JSONObject) absence.get("absence");
            String currentName = (String) absenceObject.get("lastName");
            if(currentName.equals(target)){
                sumAbsence += Integer.parseInt(String.valueOf(absenceObject.get("duration")));
                nbReport++;
            }
        }

        System.out.println(target + " a " + sumAbsence + " heures d'absences en "+ nbReport +" signalements.");

    }
}
