package nathan.piranda;

import java.util.Scanner;

public class LoginLibrary {

    /**
     *
     * @return Teacher login
     */
    public static String loginTeacher() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Login : ");
        return scanner.nextLine();
    }

    public static void loginAdmin() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Login : ");
        String login = scanner.nextLine();
    }

    /**
     *
     * @return Student credentials
     */
    public static String[] loginStudent() {
        String[] credentials = new String[3];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nom : ");
        credentials[0] = scanner.nextLine();
        System.out.println("Prénom : ");
        credentials[1] = scanner.nextLine();
        System.out.println("Groupe TP : ");
        credentials[2] = scanner.nextLine();

        return credentials;
    }
}
