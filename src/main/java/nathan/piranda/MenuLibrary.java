package nathan.piranda;

import java.util.Scanner;

public class MenuLibrary {

    /**
     * Affiche le menu de choix de profil utilisateur.
     * @return Le code du profil
     */
    public static int profileChoice(){
        System.out.println("Veuillez saisir votre profil\n\t1. Enseignant\n\t2. Scolarité\n\t3. Etudiant-Ingénieur");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Votre choix : ");
        int profile;
        do {
            profile = scanner.nextInt();
        } while (profile <= 0 || profile > 3);
        return profile;
    }

    /**
     * Affiche les fonctionnalités du profil professeur
     * @return Le choix de la fonctionnalité
     */
    public static int teacherMenu() {
        System.out.println("Veuillez saisir votre opération\n\t1. Déclarer\n\t2. Consulter un élève\n\t3. Quitter");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Votre choix : ");
        int choice;
        do {
            choice = scanner.nextInt();
        } while (choice <= 0 || choice > 3);
        return choice;
    }

    /**
     * Affiche les fonctionnalités du profil administration
     * @return Le choix de la fonctionnalité
     */
    public static int adminMenu() {
        System.out.println("Veuillez saisir votre opération\n\t1. Consulter un élève\n\t2. Consulter global\n\t3. Quitter");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Votre choix : ");
        int choice;
        do {
            choice = scanner.nextInt();
        } while (choice <= 0 || choice > 3);
        return choice;
    }

    /**
     * Affiche les fonctionnalités du profil étudiant
     * @return Le choix de la fonctionnalité
     */
    public static int studentMenu() {
        System.out.println("Veuillez saisir votre opération\n\t1. Consulter\n\t3. Quitter");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Votre choix : ");
        int choice;
        do {
            choice = scanner.nextInt();
        } while (choice <= 0 || choice > 3);
        return choice;
    }
}
