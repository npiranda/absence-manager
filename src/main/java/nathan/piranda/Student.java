package nathan.piranda;

public class Student {
    private String name;
    private String surname;
    private String group;

    /**
     *
     * @param name Student name
     * @param surname Student surname
     * @param group Student group
     */
    public Student(String name, String surname, String group) {
        this.name = name;
        this.surname = surname;
        this.group = group;
    }

    /**
     * Set Lab session group
     * @param group
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Set student name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set student surname
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return Student name
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return Student surname
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     *
     * @return Student lab session group
     */
    public String getGroup() {
        return this.group;
    }




}
